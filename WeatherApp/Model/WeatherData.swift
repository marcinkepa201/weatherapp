//
//  WeatherData.swift
//  WeatherApp
//
//  Created by Marcin Kępa on 13/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import Foundation

struct WeatherData: Codable {
    let name: String
    let main: Main
    let weather: [Weather]
    
}
struct Main: Codable {
    let temp: Double
}
struct Weather: Codable {
    let description: String
    let id: Int 
}
